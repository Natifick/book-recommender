FROM python:3.8
MAINTAINER natifick
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD ["python3", "-m", "uvicorn", "source.main:app", "--host=0.0.0.0", "--port=80"]