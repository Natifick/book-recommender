import dill
import json
from .candgen import *

class Recommender:
    """
    Класс рекомендера, описывает основные шаги генерации рекомендаций
    """
    def __init__(self, candgen_path="source/candgen.conf", ctb_path="models/ctb_model.dill"):
        """
        Сохраняем основные шаги в модели:
        Генерация кандидатов, применение модели, сортировка и обрезание данных
        @param candgen.conf: Файл со списком классов-наследников от CandidateGenerator
        @param ctb_path: Модель catboost, используемая для ранжирования
        """
        self.books = pd.read_csv("data/books.csv")
        # Нужны для заполнения пропусков
        self.mode_values = pd.read_csv("data/temporal/mode_values.csv")
        with open(candgen_path, 'r') as f:
            self.parse_conf(json.load(f))
        with open(ctb_path, 'rb') as f:
            self.ctb_model = dill.load(f)
    
    def get_user_history(self, user_id):
        table = pd.read_csv("data/ratings.csv")
        table = table[table['user_id'] == user_id]
        if table.shape[0] == 0:
            return;
        return table.merge(self.books, how='left', on='book_id').sort_values(by=['rating'], ascending=False)
    
    def parse_conf(self, conf):
        """
        Получает словарик conf и создаёт по заданной в нём схеме генераторы
        """
        candgens = []
        for k in conf.keys():
            if k == "random":
                candgens.append(RandomCandgen(*conf[k]))
            elif k == "popularity":
                candgens.append(PopularityCandgen(*conf[k]))
            elif k == "rating":
                candgens.append(RatingCandgen(*conf[k]))
            elif k == "lightfm":
                candgens.append(LightFMCandgen(*conf[k]))
        self.candgen = MixingGenerator(candgens)
    
    def recommend(self, user_ids):
        """
        Ответ на запрос на генерацию кандидатов
        @param user_ids: id пользователей, для которых необходимо сгенерировать ответ
        @return book_ids: таблица соответствия user-item
        """
        candidates = self.candgen.get_multithread(np.array(user_ids))
        candidates.drop_duplicates(subset=['user_id', 'book_id'], inplace=True)
        ratings = pd.read_csv("data/ratings.csv")
        candidates.drop(candidates[np.all(candidates[['user_id', 'book_id']]\
                                          .isin(ratings[['user_id', 'book_id']]), axis=1)].index,inplace = True)
        
        # После получения кандидатов
        feature_cols = ['id', 'book_id', 'work_id', 'books_count',
                        'isbn13', 'original_publication_year', 'average_rating', 
                        'ratings_count', 'work_ratings_count', 'work_text_reviews_count', 
                        'ratings_1', 'ratings_2', 'ratings_3', 'ratings_4', 'ratings_5']
        drop_col = ['user_id', 'book_id']
        score_feat = candidates.merge(self.books[feature_cols], on=['book_id'], how='left')
        # fillna for catboost with the most frequent value
        score_feat = score_feat.fillna(candidates.mode().iloc[0])
        score_feat.drop(["candgen"], axis=1, inplace=True)
        ctb_prediction = self.ctb_model.predict_proba(score_feat.drop(drop_col, axis=1, errors='ignore'))
        candidates['ctb_pred'] = ctb_prediction[:, 1]
        candidates = candidates.sort_values(by=['user_id', 'ctb_pred'], ascending=[True, False])
        candidates['rank_ctb'] = candidates.groupby('user_id').cumcount() + 1
        
        # как и обещали, вернём всё, что знаем про эти книги
        return candidates.merge(self.books, how='left', on='book_id')
