import pandas as pd
import numpy as np
import dill
from threading import Thread
import multiprocessing as mp
import json

class CandidateGenerator:
    """
    Базовый интерфейс генерации кандидатов для последующей рекомендации
    Каждый генератор почти наверняка связан с какой-нибудь табличкой
    При этом чтобы не держать их все в памяти, загружать лучше в момент запроса
    """
    def __init__(self, num_samples, table_path="data/temporal/"):
        """
        Запоминаем часть информации, возможно, запоминаем распределение
        Чтобы генерировать кандидатов не с нуля
        @param num_samples: количество кандидатов для генерации
        """
        pass
    
    def generate(self, user_ids, q=None):
        """
        Генерация самих кандидатов, как правило основывается на самом пользователе
        @param user_ids: пользователи, для которых необходимо сгенерировать кандидатов
        @return candidates: таблица соответствия пользователь - кандидаты
        """
        pass


class LightFMCandgen(CandidateGenerator):
    """
    Получает кандидатов на основе коллаборативной модели LightFM
    """
    def __init__(self, num_samples, table_path="data/temporal/lightfm/"):
        with open(f"models/lfm_model.dill", 'rb') as f:
            self.lfm_model = dill.load(f)
        
        with open(table_path + "lightfm_mapping.json", "r") as f:
            lightfm_mapping = json.load(f)
        lightfm_mapping['users_mapping'] = {int(k): v for k, v in lightfm_mapping['users_mapping'].items()}
        lightfm_mapping['books_mapping'] = {int(k): v for k, v in lightfm_mapping['books_mapping'].items()}
        lightfm_mapping['users_inv_mapping'] = {v: int(k) for k, v in lightfm_mapping['users_mapping'].items()}
        lightfm_mapping['books_inv_mapping'] = {v: int(k) for k, v in lightfm_mapping['books_mapping'].items()}
        
        with open(table_path + "all_cols.json", "r") as f:
            all_cols = json.load(f)
        
        with open(table_path + "known_books.json", "r") as f:
            known_books = json.load(f)
        
        self.mapper = self.generate_lightfm_recs_mapper(
            self.lfm_model,
            item_ids=all_cols,
            known_items=known_books,
            N=num_samples,
            user_features=None,
            item_features=None,
            user_mapping=lightfm_mapping['users_mapping'],
            item_inv_mapping=lightfm_mapping['books_inv_mapping'],
            num_threads=5
        )

    def generate(self, user_ids, q=None):
        """
        В случае с коллаборативной фильтрацией, у нас просто есть готовая матрица
        Поэтому мы технически не применяем алгоритм, а просто извлекаем из таблички уже готовые данные
        
        Отсюда возникает необходимость использовать что-то ещё: нет взаимодействий -> нет рекоммендаций
        """
        try:
            result = pd.DataFrame({'user_id': user_ids})
            result['book_id'] = result['user_id'].map(self.mapper)
            result = result.explode('book_id').reset_index(drop=True)
            result['rank'] = result.groupby('user_id').cumcount() + 1
            result['candgen'] = 'lightfm'
            if q is not None:
                q.put(result)
            else:
                return result
        except Exception as exc:
            print(exc)
    
    def generate_lightfm_recs_mapper(self, model, item_ids, known_items, 
                                     user_features, item_features, N, 
                                     user_mapping, item_inv_mapping, 
                                     num_threads=1):
        def _recs_mapper(user):
            user_id = user_mapping[user]
            recs = model.predict(user_id, item_ids, user_features=user_features, 
                                 item_features=item_features, num_threads=num_threads)

            additional_N = len(known_items[user_id]) if user_id in known_items else 0
            total_N = N + additional_N
            top_cols = np.argpartition(recs, -np.arange(total_N))[-total_N:][::-1]

            final_recs = [item_inv_mapping[item] for item in top_cols]
            if additional_N > 0:
                filter_items = known_items[user_id]
                final_recs = [item for item in final_recs if item not in filter_items]
            return final_recs[:N]
        return _recs_mapper


class RandomCandgen(CandidateGenerator):
    """
    Генератор, который просто всем пользователям даёт случайные книги
    Так как мы ему не сильно верим, а катбуст может опираться на ранг,
    То мы дадим всем объектам ранг равный количеству
    """
    def __init__(self, num_samples, table_path="data/temporal/"):
        self.count = num_samples
        self.list_path = table_path

    def generate(self, user_ids, q=None):
        book_ids = np.load(self.list_path + "book_ids.npy")
        result = pd.DataFrame({'user_id': np.tile(user_ids, self.count),
                           'book_id': np.random.choice(book_ids, self.count * len(user_ids)),
                           'rank': self.count, # Этому генератору мы не сильно верим -> ранг самый последний из возможных
                           'candgen': 'random_candgen'}) # полезно сохранить источник
        if q is not None:
            q.put(result.sort_values(['user_id', 'book_id']))
        else:
            return result.sort_values(['user_id', 'book_id'])


class PopularityCandgen(CandidateGenerator):
    """
    Всем отдаёт книги, которые чаще всего участвовали во взаимодействиях, с оценкой > 3
    Генератор смотрит только на количество взаимодействий:
    Книга в 100 взаимодействиях с рейтингом 3 лучше книги в 99 взаимодействиях с рейтингом 5 
    """
    def __init__(self, num_samples, table_path="data/temporal/"):
        self.count = num_samples
        self.table_path = table_path

    def generate(self, user_ids, q=None):
        top_books = pd.read_csv(self.table_path + "popular_books.csv")['book_id'].values[:self.count]
        result = pd.DataFrame({'user_id': np.tile(user_ids, self.count),
                           'book_id': np.repeat(top_books, user_ids.shape[0]),
                           'rank': np.repeat(np.arange(1, self.count + 1), user_ids.shape[0]),
                           'candgen': 'popularity_candgen'})
        if q is not None:
            q.put(result.sort_values(['user_id', 'book_id', 'rank']))
        else:
            return result.sort_values(['user_id', 'book_id', 'rank'])


class RatingCandgen(CandidateGenerator):
    """
    Возвращает книги, которые были в среднем выше всего оценены
    Опирается уже и на количество и на сам рейтинг, може быть полезнее простой популярности
    """
    def __init__(self, num_samples, table_path="data/temporal/"):
        self.count = num_samples
        self.table_path = table_path
    
    def generate(self, user_ids, q=None):
        top_books = pd.read_csv(self.table_path + "top_rating_books.csv")['book_id'].values[:self.count]
        result = pd.DataFrame({'user_id': np.tile(user_ids, self.count),
                           'book_id': np.repeat(top_books, user_ids.shape[0]),
                           'rank': np.repeat(np.arange(1, self.count + 1), user_ids.shape[0]),
                           'candgen': 'rating_candgen'})
        if q is not None:
            q.put(result.sort_values(['user_id', 'book_id', 'rank']))
        else:
            return result.sort_values(['user_id', 'book_id', 'rank'])


class ThreadedGenerator(Thread):
    def __init__(self, candgen, user_ids):
        Thread.__init__(self)
        self.candgen = candgen
        self.user_ids = user_ids
    
    def run(self):
        self.candidates = self.candgen.generate(self.user_ids)


class MixingGenerator:
    """
    Объединяет несколько кандидатогенераторов, запуская их параллельно
    """
    def __init__(self, candgens):
        self.candgens = candgens
    
    def get(self, user_ids):
        """
        Получаем кандидатов от каждого генератора последовательно
        """
        candidates = []
        for candgen in self.candgens:
            candidates.append(candgen.generate(user_ids))
        return pd.concat(candidates)
    
    def get_multithread(self, user_ids):
        """
        Получает кандидатов от каждого генератора многопоточно
        """
        threads = []
        for candgen in self.candgens:
            t = ThreadedGenerator(candgen, user_ids)
            t.start()
            threads.append(t)
        candidates = []
        for t in threads:
            t.join()
            candidates.append(t.candidates)
        return pd.concat(candidates)
    
    def get_multiprocess(self, user_ids):
        """
        Получает кандидатов в одном потоке, но в разных процессах
        """
        q = mp.Queue()
        processes = []
        for candgen in self.candgens:
            p = mp.Process(target=candgen.generate, args=(user_ids, q))
            p.start()
            processes.append(p)
        for p in processes:
            p.join()
        #q.get()
        return pd.concat(q)