from .recommender import Recommender
from fastapi import FastAPI, Request, Form
from typing import List
from fastapi.responses import RedirectResponse, HTMLResponse
from fastapi.templating import Jinja2Templates
import numpy as np
import pandas as pd


app = FastAPI()
templates = Jinja2Templates(directory="templates")


@app.on_event("startup")
async def startup():
    app.state.recommender = Recommender()
    #book_counts = {user_id: app.state.recommender.recommend([int(user_id)]).shape[0] for user_id in np.random.choice(10_000, size=100)}
    #print(book_counts, np.mean(list(book_counts.values())), np.min(list(book_counts.values())), np.max(list(book_counts.values())))


@app.get("/ban_genres/")
async def genre_banner(genre_ban_list: List[str] = Form(default=[])):
    print("trying to ban")
    for i in genre_ban_list:
        print(i)


@app.get("/main_page/{user_id}", response_class=HTMLResponse)
async def main_page(request: Request, user_id: int):
    try:
        books = app.state.recommender.recommend([int(user_id)])
        books = books[["id", "title", "authors", "image_url", "rank_ctb"]]
        books = books.rename(columns={'rank_ctb': 'rank'}).to_dict(orient='records')
        books = [books[i * 5 : (i + 1) * 5] for i in range(len(books) // 5)]
        genres = pd.read_csv("data/temporal/genres_popularity.csv")["name"].values.tolist()
        return templates.TemplateResponse("index.html", {"request": request, "books": books, "genres": genres})
    except Exception as exc:
        print(exc)
        response = RedirectResponse(url=f"/recommend_fallback/{user_id}")
        return response


@app.get("/history/{user_id}", response_class=HTMLResponse)
async def main_page(request: Request, user_id: int):
    try:
        books = app.state.recommender.get_user_history(user_id)
        if books is None:
            print("user has no books")
            response = RedirectResponse(url=f"/recommend_fallback/{user_id}")
            return response
        books = books[["id", "title", "authors", "image_url", "rating"]]
        # Костыль, чтобы использовать 1 шаблон для рекомендаций и для истории
        # Возможно, стоит потом исправить поэлегантнее
        books = books.rename(columns={'rating': 'rank'}).to_dict(orient='records')
        books = [books[i * 5 : (i + 1) * 5] for i in range(len(books) // 5)]
        genres = pd.read_csv("data/temporal/genres_popularity.csv")["name"].values.tolist()
        return templates.TemplateResponse("index.html", {"request": request, "books": books, "genres": genres})
    except Exception as exc:
        print(exc)
        response = RedirectResponse(url=f"/recommend_fallback/{user_id}")
        return response


@app.get("/recommend/{user_id}")
async def recommend(user_id: int):
    try:
        books = app.state.recommender.recommend([int(user_id)])
        return {'books': books['book_id'].values.tolist()}
    except Exception as exc:
        print(exc)
        response = RedirectResponse(url=f"/{user_id}/recommend_fallback")
        return response


@app.get("/recommend_fallback/{user_id}")
async def recommend_fallback(user_id: int):
    return {"books": "Something went wrong, sooorry, no books for now"}
